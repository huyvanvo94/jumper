package com.hv.jumper;
public class Box {
    boolean hide = false;
    public final Integer mColor  ;

    public final int mHeight ;
    public final int mWidth ;
    public int mX ;
    public int mY ;

    public Box(Integer color, int x, int y, int width, int height){
        mColor = color;
        mWidth = width;
        mHeight = height;
        mX = x;
        mY = y ;
    }

    public boolean contains(int x, int y){
        return mX <= x && x <= mX + mWidth &&
                mY <= y && y <= mY + mHeight;
    }

    @Override
    public int hashCode(){
        int hash = 7 ;

        hash = 31 * hash + mColor;
        hash = 31 * hash + mHeight;
        hash = 31 * hash + mWidth;
        hash = 31 * hash + mX;
        hash = 31 * hash + mY;

        return hash;

    }
}