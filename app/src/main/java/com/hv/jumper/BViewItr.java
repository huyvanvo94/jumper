package com.hv.jumper;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import java.util.List;
import java.util.Random;

/*
Creates next box view
 */
public class BViewItr {
    // by default all
    int x = 0 ;

    public static final int M_BOXES = 5;

    int mY = 0 ;
    int mX = 0 ;
    int N ;
    int mWidth ;
    int mHeight;

    Bitmap mBitmap ;

    public BViewItr(int width, int height, int count){
        mWidth = width;

        mHeight = height;
        N = mWidth / count;
    }

    public static Random rand = new Random();

    public final int[] COLORS = {
            Color.RED, Color.BLUE,
            Color.YELLOW, Color.GREEN, Color.CYAN
    };

    public Box next(List<Integer> colors){
      //  Log.i(BViewItr.class.getSimpleName(), "-----------"+mX+"----------------"+N);
        int x = rand.nextInt(colors.size());
        int color = colors.get(x);
        colors.remove(x);


        Box config =
                new Box(color,
                        mX,
                        mHeight,
                        N,
                        N);


        mX = (mX + N) % mWidth;
        return config;
    }
}
