package com.hv.jumper;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
Update the views
 */
public class BoxViewsManager implements BoxViewListener {
    public static final int M_BOXES = 5;

    // Used to sync
    Object object = new Object();

    public static Random rand = new Random();
    boolean forever = false;

    static List<Integer> COLORS = new ArrayList<>();
    static {
        COLORS.add(Color.RED);
        COLORS.add(Color.BLUE);
        COLORS.add(Color.YELLOW);
        COLORS.add(Color.GREEN);
        COLORS.add(Color.CYAN);

    }


    final Integer mHeight;
    final Integer mWidth;

    final Context mContext;
    final LinearLayout mLinearLayout;
    final List<Box> mBoxes = new ArrayList<>();

    final BViewItr mBViewIter;
    JumpDrawer view;

    public BoxViewsManager(Context context, LinearLayout linearLayout, Integer width, Integer height){
        mContext = context;
        mLinearLayout = linearLayout;
        mHeight = height;
        mWidth = width;

        mBViewIter = new BViewItr(mWidth, mHeight, M_BOXES);
        view = new JumpDrawer(mContext);
        mLinearLayout.addView(view);
        view.mBoxViewListener = this;
        view.setData(mBoxes);

    }


    public void init( ){
        mBoxes.clear();
        view.count = 0;

        newTiles();
        start();


    }

    public void newTiles(){

        List<Integer> COLORS = new ArrayList<>();

        COLORS.add(Color.RED);
        COLORS.add(Color.BLUE);
        COLORS.add(Color.YELLOW);
        COLORS.add(Color.GREEN);
        COLORS.add(Color.CYAN);

        for (int i = 0; i < M_BOXES; i++)
            view.add(mBViewIter.next(COLORS));
    }


    @Override
    public void maxValue(int min) {
        //Log.d(BoxViewsManager.class.getSimpleName(), min+"");

        int offset = mHeight - (mWidth/M_BOXES);

        if(min <= offset){

            newTiles();
        }


    }

    @Override
    public void onTouch(int x, int y) {
        synchronized (mBoxes) {
            for (Box b : mBoxes)
                if (b.contains(x, y)) {
                    b.hide = true;
                    break;

                }
        }
    }


    public void reset(){
        forever=false;



        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                init();
            }
        });


    }

    public void start(){

        forever = true;

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                while (forever){
                    synchronized (mBoxes){
                        Log.d(BoxViewsManager.class.getSimpleName(), "run thread");
                        int max = Integer.MIN_VALUE;

                        for(Box b: mBoxes){
                            b.mY--;

                            if (b.mY <= 0) {
                                reset();
                                break;
                            }

                            max = Math.max(max, b.mY);

                        }
                        maxValue(max);
                    }

                    view.updateUI();

                    try {

                        Thread.sleep(2);
                    } catch (Exception e) {
                    }
                }

            }
        });


        t.start();

    }
}
