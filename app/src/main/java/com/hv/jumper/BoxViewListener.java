package com.hv.jumper;

public interface BoxViewListener {

    void maxValue(int min);

    void onTouch(int x, int y);
}
