package com.hv.jumper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class JumpDrawer extends View {

    public static int generation = 0;

    int count = 0 ;


    Context mContext;
    List<Box> mBoxes = new ArrayList<>();

    final Object object = new Object();
    public BoxViewListener mBoxViewListener;

   // Contants.GameConfig config = new Contants.GameConfig;

    public JumpDrawer(Context context){
        super(context);
        mContext = context;
    }



    public void setData(List<Box> data){
        mBoxes = data;
    }

    public void add(Box c){
        mBoxes.add(c);

    }

    static Paint paint = new Paint();
    static {
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
      //  paint.setColor(Color.BLACK);
        paint.setTextSize(100);
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

        synchronized (object) {
            synchronized (mBoxes) {

                Iterator<Box> iter = mBoxes.iterator();

                while (iter.hasNext()) {
                    Box b = iter.next();

                    if (b.hide) {
                        count++;
                        iter.remove();


                    } else {

                        canvas.drawRect(rect(b.mX, b.mY, b.mHeight), getPaint(b.mColor));
                    }
                }


                canvas.drawText("Points: " + count, 100, 100, paint);
                Log.d(JumpDrawer.class.getSimpleName(), mBoxes.size() + "");
            }
        }

    }

    public boolean onTouchEvent(MotionEvent event){

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                synchronized (object) {
                    Log.d(JumpDrawer.class.getSimpleName(), "onTouchEvent");

                    final int x = (int) event.getX();
                    final int y = (int) event.getY();

                    if(mBoxViewListener != null){
                        mBoxViewListener.onTouch(x, y);
                    }


                }


                break;


        }
        return true;
    }

    /*
    public void start() {
        forever = true;
        final JumpDrawer who = this;
        @SuppressLint("HandlerLeak")
        final Handler handler = new Handler();


        new Thread(new Runnable() {
            @Override
            public void run() {
                while (forever){

                    synchronized (object) {

                        int max = Integer.MIN_VALUE;

                        synchronized (mBoxes) {
                            for (Box b : mBoxes) {
                                b.mY--;

                                if (b.mY <= 0) {
                                    if (mBoxViewListener != null)
                                        mBoxViewListener.oops();
                                }

                                max = Math.max(max, b.mY);


                            }

                            if (who.mBoxViewListener != null)
                                who.mBoxViewListener.maxValue(max);
                        }
                    }


                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            who.invalidate();

                        }
                    });
                    try {
                        Log.d(JumpDrawer.class.getSimpleName(), "from " + generation);
                        Thread.sleep(2);
                    } catch (Exception e) {
                    }

                }

            }
        }).start();

    }*/


    /*
    public void stop(){
        forever = false;
    } */

    public synchronized void updateUI(){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                JumpDrawer.this.invalidate();
            }
        });
    }

    public Rect rect(int x, int y, int N){
        return new Rect(x, y, N+x, N+y);
    }

    public Paint getPaint(int c) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor( c );
        return paint;
    }



}
