package com.hv.jumper;

public final class Contants {

    public final static class GameConfig {
        final int EASY = 1000;
        final int MEDIUM = 100;
        final int HARD = 10;


        int level = EASY;

        public static GameConfig getGameConfig(int x){
            GameConfig gameConfig = new GameConfig();
            gameConfig.level = x;

            return gameConfig;
        }
    }
}
